# import and variables declares
# NO RETURNS, NO BREAKS!

import random
import dice

die1 = random.randint(1,6)
die2 = random.randint(1,6)
die3 = random.randint(1,6)
die4 = random.randint(1,6)
die5 = random.randint(1,6)

timesRolled = 0

# keep track of current rounds
currentWin  = 0
currentLose = 0

# keep track of streaks for summary display
longestWin  = 0
longestLose = 0

# function to process user input
# and log correct responses
def process_result(userInput):

    # get reference to global variable to avoid "local variable referenced before assignment" error
    global currentWin, currentLose, longestWin, longestLose

    # will calculate the correct answer by looping through array of dices
    dices = [die1, die2, die3, die4, die5]
    answer = 0

    for i in dices:
        if i == 3:
            answer += 2
        elif i == 5:
            answer += 4

    # compare with users answer
    # CORRECT ANSWER
    if int(userInput) == answer:

        # update streaks
        currentWin += 1
        currentLose = 0

        # check if current win is higher than streak, reassign if yes
        if currentWin > longestWin:
            longestWin = currentWin

        print("yay")

    # WRONG ANSWER
    else:
        # update streaks
        currentLose += 1
        currentWin   = 0

        # check if current lose is higher than streak, reassign if yes
        if currentLose > longestLose:
            longestLose = currentLose

        # if answer is an ODD number
        if int(userInput) % 2 != 0:

            # tells user the correct answer vs their wrong answer
            print("The answer is:", answer, "not", userInput)
            print("Its always zero or even number\n")

        # if answer is EVEN but still wrong
        else:
            print("Wrong!")

        # show hint if lose streak > 5 || current lose > 5?
        if currentLose >= 5:
            print("which dice looks like a flower?\n")


# GAME STARTS!
print("Game intro here\n")

# ask user if they want to play
gameOn = str(input("play mou? [y|n]\n"))


# main game flow when started
while gameOn == "y":
    # increment rolling time
    timesRolled += 1

    # show dices
    print(dice.display_dice(die1,die2,die3,die4,die5))

    # get user input and process
    userGuess = str(input("enter your guess: [numbers only!]\n"))
    process_result(userGuess)

    # check if users wants to go again
    gameOn = str(input("play again? [y|n]\n"))

    # if YES
    if gameOn == "y":
        timesRolled += 1
        die1 = random.randint(1,6)
        die2 = random.randint(1,6)
        die3 = random.randint(1,6)
        die4 = random.randint(1,6)
        die5 = random.randint(1,6)

    # if NO
    else:
        # show game summary
        print("\ngame summary here:")
        print("highest win streak:\t", longestWin, "\nhighest lose streak:\t", longestLose, "\nRolled:\t", timesRolled)

# Outside of while loop
# if user doesnt want to start game at all
if timesRolled == 0:
    print("then not.")
